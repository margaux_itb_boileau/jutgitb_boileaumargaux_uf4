/*
* AUTHOR: Margaux Boileau
* DATE: 2023/02/22
* TITLE: jutgITB
* VERSION: 1.0
*/

import java.io.File
import java.time.LocalDate
import java.util.*
import kotlin.system.exitProcess

val scanner = Scanner(System.`in`)
lateinit var conjunt: MutableList<MutableList<Problema>>

fun main() {
    val dades = creaLlistaProblemes("dades")
    val condicionals = creaLlistaProblemes("condicionals")
    val bucles = creaLlistaProblemes("bucles")
    val llistes = creaLlistaProblemes("llistes")
    conjunt = mutableListOf(dades, condicionals, bucles, llistes)
    instruccions()
    menu()
}

fun creaLlistaProblemes(fitxer:String): MutableList<Problema> {
    val llistatProblemes = mutableListOf<Problema>()
    val llista = File("./src/main/kotlin/problemes/$fitxer").readText().split("#")
    for (problema in llista) {
        val prob = problema.split("~")
        val newProblema = Problema(prob[0],prob[1],JocProves(prob[2],prob[3]),JocProves(prob[4],prob[5]),0, mutableListOf(),false,"$fitxer")
        llistatProblemes.add(newProblema)
    }
    return llistatProblemes
}

fun menu() {
    println()
    println("Què vols fer?\n"+
            "1. Seguir amb l’itinerari d’aprenentatge\n" +
            "2. Llista problemes\n" +
            "3. Consultar històric de problemes\n" +
            "4. Ajuda\n" +
            "5. Sortir\n")
    do {
        val instruccio = scanner.nextInt()
        when (instruccio) {
            1-> continuar()
            2-> llistat()
            3-> historial()
            4-> instruccions()
            5-> exitProcess(0)
        }
        menu()
    } while (instruccio!=5)
}

fun continuar() {
    for (llista in conjunt) {
        for (problema in llista){
            if (problema.estat==false) logica(problema)
        }
    }
}

fun llistat() {
    for (llista in conjunt) {
        println(llista[0].tipus.uppercase())
        for (prolema in llista){
            println(prolema.titol)
        }
        println()
    }
    println("A quin problema vols accedir?")
    do {
        val instruccio = scanner.next()
        val llista = instruccio.split('.')[0].toInt()-1
        val exercici = instruccio.split('.')[1].toInt()-1
        val problema = conjunt[llista][exercici]
        if (problema.estat) println("Aquest problema ja està resolt, escull un altre")
        else {
            logica(problema)
            println("A quin problema vols accedir?")
        }
    } while (true)

}

fun historial() {
    for (llista in conjunt) {
        println(llista[0].tipus.uppercase())
        for (prolema in llista){
            println(prolema.titol)
            println("\tIntents: ${prolema.intents}")
            if (prolema.estat) println("\tResolt: si")
            else println("\tResolt: no")
        }
        println()
    }
}

fun instruccions() {
    println("Benvingut al JutgITB, a continuació tindràs una serie de problemes de programació de diferents tipus(tipus de dades, condicionals...). " +
            "\nCada problema té un joc de proves públic i un privat, per resoldre'l hauras d'escriure per terminal la solució del teu programa en passar-li l'entrada del joc privat." +
            "\nA traves del menu principal pots resoldre els problemes per ordre, escollir-los individualment i veure l'historial dels problemes." +
            "\nPer navegar dins del menú i escollir les accions que vols has d'introduir els números que corresponen a l'opció escollida.")
}

fun logica(problema: Problema) {
    println(problema.titol)
    println(problema.enunciat)
    println("Entrada: ${problema.jocPubl.input}")
    println("Sortida: ${problema.jocPubl.ouput}")
    println()
    println("Vols resoldre'l? (SI / NO)")
    var resposta = scanner.next()
    if (resposta == "no") menu()
    do {
        problema.intents++
        println("Entrada: ${problema.jocPriv.input}")
        resposta = scanner.next()
        if (resposta!=problema.jocPriv.ouput) {
            problema.llistaIntents.add(Intent(resposta, LocalDate.now().toString()))
            println("Resposta incorrecta")
            println("\t1.Tornar a intentar\n\t2.Següent exercici\n\t3.Tornar al menu")
            val instruccio = scanner.nextInt()
            when(instruccio){
                2-> break
                3->menu()
            }
        }
        else {
            problema.estat=true
            println("Resposta correcta")
            println("\t1.Següent exercici \n\t2.Tornar al menu")
            val instruccio = scanner.nextInt()
            when(instruccio){
                1-> break
                else -> menu()
            }
        }
    } while (true)
}